$(document).ready(
    function(){
        $(".accordion-content:first").slideDown();

        $("#accordion .accordion-header").click(
            function(){
                var content = $(this).parent().next(".accordion-content");
                content.slideDown();
                var item = $(this).parent().parent(".accordion-item");
                item.nextAll(".accordion-item").children(".accordion-content").slideUp();
                item.prevAll(".accordion-item").children(".accordion-content").slideUp();
            }
        );

        $("#accordion .sd").click(
            function(){
                var item = $(this).parent().parent().parent();
                item.insertAfter(item.next());
            }
        );

        $("#accordion .su").click(
            function(){
                var item = $(this).parent().parent().parent();
                item.insertBefore(item.prev());
            }
        );
    }
);