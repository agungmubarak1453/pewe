$(document).ready(
    function(){
        var bookSelf = $('#bookself tbody');

        var search = function(query){
            $.ajax({
                url: '/story8/searchbook',
                data:{
                    'query' : query
                },
                dataType: 'json',
                success: function(response){
                    bookSelf.empty();
                    var items = response['items'];
                    if(!items){
                        $('#info').slideDown();
                        return;
                    }
                    $('#info').slideUp();
                    for(item of items){
                        var info = item['volumeInfo'];
        
                        var title = info['title'];
                        var subtitle = info['subtitle'];
                        var authors = info['authors'];
                        var image = info['imageLinks'];

                        if(image != undefined){
                            image = image['thumbnail']
                        }
                        var description = info['description'];
                        var link = info['infoLink'];
                        
                        var book = $('<tr class="book-item"><td></td><td></td><td></td><td></td></tr>').appendTo(bookSelf);
                        var column = book.children();

                        if(image != undefined){
                            $('<img src="' + image + '">').appendTo(column[0]);
                        }else{
                            $("<p>gambar tidak ditemukan</p>").appendTo(column[0]);
                        }

                        $('<h4><a target="_blank" href="' + link + '">'+ title + '</a></h4>').appendTo(column[1]);
                        if(subtitle != undefined){
                            $('<p>'+ subtitle + '</p>').appendTo(column[1]);
                        }

                        if(authors != undefined){
                            $('<p>' + authors + '</p>').appendTo(column[2]);
                        }else{
                            $("<p>penulis tidak ditemukan</p>").appendTo(column[2]);
                        }

                        if(description != undefined){
                            $('<p>' + description + '</p>').appendTo(column[3]);
                        }else{
                            $("<p>-</p>").appendTo(column[3]);
                        }
                    }
                }
            });
        }    

        $('#info').slideUp();
        $('#searchbox').val('Elsa');
        search('Elsa');

        $('#searchbox').keydown(function(){
            var query = $(this).val();
            search(query);
        });
    }
);