from django.shortcuts import render

# Create your views here.

ttl = 'Pewe'

def home(request):
    response = {'ttl' : ttl}
    return render(request, 'home.html', response)

def profile(request):
    response = {'ttl' : ttl + ' It\'s me!', 'title' : 'It\'s Me!'}
    return render(request, 'profile.html', response)

def work(request):
    response = {'ttl' : ttl + ' My Work'}
    return render(request, 'work.html', response)