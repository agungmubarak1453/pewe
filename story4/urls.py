from django.urls import include, path
from .views import *

app_name = 'story4'
urlpatterns = [
    path('', home, name='home'),
    path('work', work, name='work'),
    path('profile', profile, name='profile'),
]