from django.urls import include, path
from .views import *

app_name = 'coursetaked'
urlpatterns = [
    path('', home, name='home'),
    path('courseform', courseform, name='courseform'),
    path('coursedetail/<str:course_name>', coursedetail, name="coursedetail"),
    path('removecourse/<str:course_name>', removecourse, name="removecourse"),
    path('savecourse', savecourse),
]