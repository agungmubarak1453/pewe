from django import forms
from .models import *
from django.core.exceptions import ValidationError

class Input_Form(forms.ModelForm):
	class Meta:
		model = Course
		fields = ['course_name', 'instructor', 'semester', 'description']

	error_messages = {
		'required' : 'Please Type'
	}
	
	def clean_semester(self):
		semester = self.cleaned_data.get('semester')
		if semester < 1:
			raise ValidationError('Zero semester is not exist')
		return semester