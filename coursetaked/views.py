from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import *

# Create your views here.
ttl = 'Pewe'
response = {}

def home(request):
	courses = Course.objects.all()
	response = {'ttl' : ttl + ' Course Taked List'
				,'title' : 'Course Taking List'
				, 'courses' : courses}
	return render(request, 'courselist.html', response)

def courseform(request):
	message = request.GET.get('message', '')
	response = {'ttl' : ttl + ' Course Taked Form'
					,'title' : 'Course Taking Form'
					,'input_form' : Input_Form
					, 'message' : message}
	return render(request, 'formcourse.html', response)

def savecourse(request):
	form = Input_Form(request.POST or None)
	if (form.is_valid() and request.method == 'POST'):
		form.save()
		return HttpResponseRedirect('/coursetaked/courseform?message=' + form.cleaned_data['course_name'] + ' have been added')
	else:
		response = {'ttl' : ttl + ' Course Taked Form'
					,'title' : 'Course Taking Form'
					,'input_form' : form
					, 'message' : 'Your input is invalid'}
		return render(request, 'formcourse.html', response)

def coursedetail(request, course_name):
	course = Course.objects.get(course_name=course_name)
	response = {'ttl' : ttl + ' Course Detail'
				,'title' : 'Course Detail'
				,'course' : course}
	return render(request, 'coursedetail.html', response)

def removecourse(request, course_name):
	Course.objects.get(course_name=course_name).delete()
	return HttpResponseRedirect('/coursetaked')