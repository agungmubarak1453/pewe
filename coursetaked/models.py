from django.db import models
from django.core.validators import MinValueValidator

class Course(models.Model):
	course_name = models.CharField(max_length=30, primary_key=True)
	instructor = models.CharField(max_length=30)
	semester = models.PositiveIntegerField(validators=[MinValueValidator(1)])
	description = models.TextField(max_length=100, blank=True)