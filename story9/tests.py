from django.test import TestCase
from django.test import Client

from .views import *
from .models import *
from .forms import *

from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout

class accountTest(TestCase):
	# URL test
	def test_url_home_exist(self):
		response = Client().get('/story9/')
		self.assertEqual(response.status_code, 200)

	def test_url_signup_exist(self):
		response = Client().get('/story9/signup')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/story9/signup-login')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/story9/signupUser-login')
		self.assertEqual(response.status_code, 200)

	def test_url_login_exist(self):
		response = Client().get('/story9/login')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/story9/login-signup')
		self.assertEqual(response.status_code, 200)

	# Template test
	def test_template_home_correct(self):
		response = Client().get('/story9/')
		self.assertTemplateUsed(response, 'home.html')

	def test_template_signup_correct(self):
		response = Client().get('/story9/signup')
		self.assertTemplateUsed(response, 'register.html')
		response = Client().get('/story9/signup-login')
		self.assertTemplateUsed(response, 'register.html')
		response = Client().get('/story9/signupUser-login')
		self.assertTemplateUsed(response, 'register.html')
		
	def test_template_login_correct(self):
		response = Client().get('/story9/login')
		self.assertTemplateUsed(response, 'loginform.html')
		response = Client().get('/story9/login-signup')
		self.assertTemplateUsed(response, 'loginform.html')

	# Model test
	def test_model_user_created(self):
		User.objects.create_user(username='user',email='email@gmail.com',password='password')
		numberModel = User.objects.all().count()
		self.assertEqual(numberModel, 1)

	# Form test
	def test_form_signup_working(self):
		form_data = {
			'username' : 'user',
			'name' : 'User Web',
			'email' : 'email@gmail.com',
			'password' : 'password',
			'repeat' : 'password',
		}
		response_post = Client().post(path='/story9/signup', data=form_data, follow=True)
		html_response = response_post.content.decode('utf8')
		self.assertIn('Halo User', html_response)

	def test_form_signup_constrain(self):
		User.objects.create_user(username='user',email='email@gmail.com',password='password')

		form_data = {
			'username' : 'user',
			'name' : 'User Web',
			'email' : 'email@gmail.com',
			'password' : 'password',
			'repeat' : 'password2',
		}
		response_post = Client().post(path='/story9/signup', data=form_data)
		html_response = response_post.content.decode('utf8')
		self.assertIn('username have been used', html_response)
		self.assertIn('email have been used', html_response)
		self.assertIn('repeat password is different', html_response)

		form_data = {
			'username' : 'user 2',
			'name' : 'User Web',
			'email' : 'email2@gmail.com',
			'password' : 'password',
			'repeat' : 'password',
		}
		response_post = Client().post(path='/story9/signup', data=form_data)
		html_response = response_post.content.decode('utf8')
		self.assertIn('alphanumeric', html_response)

	def test_form_login_working(self):
		user = User.objects.create_user(username='user',email='email@gmail.com',password='password')
		user.first_name = 'User'
		user.last_name = 'Web'
		user.save()

		form_data = {
			'username' : 'user',
			'password' : 'password'
		}
		response_post = Client().post(path='/story9/login', data=form_data, follow=True)
		html_response = response_post.content.decode('utf8')
		self.assertIn('Halo User', html_response)

	def test_form_login_constrain(self):
		User.objects.create_user(username='user',email='email@gmail.com',password='password')

		form_data = {
			'username' : 'user 2',
			'password' : 'password'
		}
		response_post = Client().post(path='/story9/login', data=form_data)
		html_response = response_post.content.decode('utf8')
		self.assertIn('alphanumeric', html_response)
		
		form_data = {
			'username' : 'user2',
			'password' : 'password'
		}
		response_post = Client().post(path='/story9/login', data=form_data)
		html_response = response_post.content.decode('utf8')
		self.assertIn('found', html_response)
		
		form_data = {
			'username' : 'user',
			'password' : 'password2'
		}
		response_post = Client().post(path='/story9/login', data=form_data)
		html_response = response_post.content.decode('utf8')
		self.assertIn('password is wrong', html_response)