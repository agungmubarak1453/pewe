from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout
from .forms import *
from django.shortcuts import redirect

# Create your views here.
ttl = 'Pewe'
def register(request, username, backurl):
	response = {
		'ttl' : ttl + 'Register',
		'title' : 'Register',
		'backurl' : backurl,
		}

	if(request.method == 'POST'):
		form = Register_Form(request.POST)
		if(form.is_valid()):
			login(request, form.save())
			return redirect("/")
	else:
		form = Register_Form(initial={'username' : username})

	response['input_form'] = form
	return render(request, 'register.html', response)

def loginuser(request, backurl):
	response = {
		'ttl' : ttl + 'Login',
		'title' : 'Login',
		'backurl' : backurl,
		}

	if(request.method == 'POST'):
		form = Login_Form(request.POST)
		if(form.is_valid()):
			cleaned_data = form.cleaned_data
			user = authenticate(request, username=cleaned_data['username'], password=cleaned_data['password'])
			login(request, user)
			return redirect("/")
		else:
			response['username'] = form.data['username']
	else:
		form = Login_Form()

	response['input_form'] = form
	return render(request, 'loginform.html', response)

def logoutuser(request):
	logout(request)
	return redirect('/')