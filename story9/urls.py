from django.urls import include, path
from .views import *
from story4.views import home

app_name = 'story9'
urlpatterns = [
    path('', home, name='home'),
    path('signup<str:username>-<str:backurl>', register, name='signup'),
    path('signup-<str:backurl>', register, {'username' : ''},name='signup'),
    path('signup', register, {'username' : '', 'backurl' : '/'},name='signup'),
    path('login-<str:backurl>', loginuser, name='login'),
    path('login', loginuser, {'backurl' : '/'}, name='login'),
    path('logout', logoutuser, name='logout'),
]