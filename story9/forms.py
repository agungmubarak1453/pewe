from django import forms
from django.contrib.auth.models import User
import re

class Register_Form(forms.Form):
	username = forms.CharField(max_length=30,required=True)
	name = forms.CharField(max_length=30,required=True)
	email = forms.EmailField(required=True)
	password = forms.CharField(required=True, widget=forms.PasswordInput)
	repeat = forms.CharField(required=True, widget=forms.PasswordInput)

	def first_last_name(self):
		flname = self.cleaned_data['name'].split(" ",1)
		return flname
		
	def clean(self):
		cleaned_data = super(Register_Form, self).clean()

		pattern = re.compile('^[a-zA-Z0-9]*$')
		if pattern.match(cleaned_data['username']):
			usernameCheck = User.objects.filter(username=cleaned_data['username']).first()
			if usernameCheck is not None:
				self.add_error('username', 'Your username have been used')
		else:
			self.add_error('username', 'Username must alphanumeric')

		emailCheck = User.objects.filter(email=cleaned_data['email']).first()
		if emailCheck is not None:
			self.add_error('email', 'Your email have been used')

		if cleaned_data['password'] != cleaned_data['repeat']:
			self.add_error('repeat', 'Your repeat password is different')
		
		return cleaned_data

	def save(self):
		flname = self.first_last_name()
		cleaned_data = self.cleaned_data
		
		user = User.objects.create_user(username=self.cleaned_data['username'],email=cleaned_data['email'],password=cleaned_data['password'])
		user.first_name = flname[0]
		if(len(flname) > 1):
			user.last_name = flname[1]
		user.save()
		return user

class Login_Form(forms.Form):
	username = forms.CharField(max_length=30,required=True)
	password = forms.CharField(required=True, widget=forms.PasswordInput)

	def clean(self):
		cleaned_data = super(Login_Form, self).clean()

		pattern = re.compile('^[a-zA-Z0-9]*$')
		if pattern.match(cleaned_data['username']):
			usernameCheck = User.objects.filter(username=cleaned_data['username']).first()
			if usernameCheck is None:
				self.add_error('username', "Your username haven't found")
			else:
				if not usernameCheck.check_password(cleaned_data['password']):
					self.add_error('password', 'Your password is wrong')
		else:
			self.add_error('username', 'Username must alphanumeric')
		
		return cleaned_data