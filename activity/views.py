from django.shortcuts import render
from .forms import *
from django.http import HttpResponseRedirect
from .models import *


# Create your views here.

ttl = 'Pewe'
response = {}

def home(request):
	activities = Activity.objects.all()
	response = {'ttl': ttl + ' Activities'
					, 'activities': activities
					, 'title': 'Activities'}
	return render(request, 'activities.html', response)


def activityform(request):
	message = request.GET.get('message', '')
	response = {'ttl': ttl + ' Activity Form'
					, 'title': 'Activity Form'
					, 'message': message
					, 'action': 'saveactivity'
					, 'input_form': Activity_Form}
	return render(request, 'form.html', response)


def saveactivity(request):
	form = Activity_Form(request.POST or None)
	if (form.is_valid() and request.method == 'POST'):
		form.save()
		return HttpResponseRedirect('activityform?message=' + form.cleaned_data['activity_name'] + ' succesfully have been added')
	else:
		response = {'ttl': ttl + ' Activity Form'
						,'title': 'Activity Form'
						,'action': 'saveactivity'
						, 'message' : 'Your input is invalid'
						, 'input_form': form}
		return render(request, 'form.html', response)

def deleteActivity(request, activity_name):
	Activity.objects.get(activity_name=activity_name).delete()
	return HttpResponseRedirect('/activity/')

def memberform(request, activity_name):
	message = request.GET.get('message', '')
	response = {'ttl': ttl + ' Member Form'
					,'title': 'Member Form'
					, 'activity_name' : activity_name
					, 'message': message
					, 'input_form': Member_Form}
	return render(request, 'memberform.html', response)

def savemember(request, activity_name):
	form = Member_Form(request.POST or None)
	if (form.is_valid() and request.method == 'POST'):
		name = form.cleaned_data.get('name')

		member = None
		try:
			member = Member.objects.get(name=name)
		except:
			member = Member.objects.create(name=name)

		activity = Activity.objects.get(activity_name=activity_name)
		try:
			MemberActivity.objects.create(name=member, activity=activity)
		except:
			form.nameError(msg="Member have been added before")
			response = {'ttl': ttl + ' Member Form'
					,'title': 'Member Form'
					, 'activity_name' : activity_name
					, 'message' : 'Your input is invalid'
					, 'input_form': form}
			return render(request, 'memberform.html', response)

		message = name + ' succesfully have been added'
		response = {'ttl': ttl + ' Member Form'
					,'title': 'Member Form'
					, 'activity_name' : activity_name
					, 'message': message
					, 'input_form': Member_Form}
		return render(request, 'memberform.html', response)

	else:
		response = {'ttl': ttl + ' Member Form'
					,'title': 'Member Form'
					, 'activity_name' : activity_name
					, 'message' : 'Your input is invalid'
					, 'input_form': form}
		return render(request, 'memberform.html', response)

def deleteMember(request, activity_name, member_name):
	MemberActivity.objects.get(name=member_name, activity=activity_name).delete()
	return HttpResponseRedirect('/activity/')