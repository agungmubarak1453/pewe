# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from .forms import *
    
# Create your tests here.
class activitytest(TestCase):

    def test_home_urls_exist(self):
        response = Client().get('/activity/')
        self.assertEqual(response.status_code, 200)

    def test_home_is_render_right(self):
        response= Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn('Activities', html_response)

    def test_model_Member_working(self):
        new_model = Member.objects.create(name='Agung')

        counting_all_available_model = Member.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_Activity_working(self):
        new_model = Activity.objects.create(activity_name='Pantai', description='')

        counting_all_available_model = Activity.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)

    def test_model_MemberActivity_working(self):
        member = Member.objects.create(name='Agung')
        activity =  Activity.objects.create(activity_name='Pantai', description='')

        new_model = MemberActivity.objects.create(name=member, activity=activity)

        counting_all_available_model = MemberActivity.objects.all().count()
        self.assertEqual(counting_all_available_model, 1)
    
    def test_form_activity_template(self):
        response= Client().get('/activity/activityform')
        html_response = response.content.decode('utf8')
        self.assertIn('Description', html_response)

    def test_form_member_template(self):
        response= Client().get('/activity/memberform-nguli')
        html_response = response.content.decode('utf8')
        self.assertIn('Name', html_response)

    def test_form_activity_working(self):
        form_data = {'activity_name': 'agung', 'description':''}
        form = Activity_Form(data=form_data)
        self.assertTrue(form.is_valid())

    def test_form_member_working(self):
        form_data = {'name': 'agung'}
        form = Member_Form(data=form_data)
        self.assertTrue(form.is_valid())

    def test_home_data_show_right(self):
        member = Member.objects.create(name='Agung')
        member2 = Member.objects.create(name='Ihsan')
        activity =  Activity.objects.create(activity_name='Pantai', description='Fun Banget')

        new_model = MemberActivity.objects.create(name=member, activity=activity)
        new_model = MemberActivity.objects.create(name=member2, activity=activity)

        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn('Pantai', html_response)
        self.assertIn('Agung', html_response)
        self.assertIn('Ihsan', html_response)
        self.assertIn('Fun Banget', html_response)

    def test_home_data_show_right_description(self):
        member = Member.objects.create(name='Agung')
        member2 = Member.objects.create(name='Ihsan')
        activity =  Activity.objects.create(activity_name='Pantai', description='')

        new_model = MemberActivity.objects.create(name=member, activity=activity)
        new_model = MemberActivity.objects.create(name=member2, activity=activity)

        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn('Pantai', html_response)
        self.assertIn('Agung', html_response)
        self.assertIn('Ihsan', html_response)
        self.assertNotIn('Description', html_response)

    def test_home_data_show_right_member(self):
        activity =  Activity.objects.create(activity_name='Pantai', description='Fun Banget')

        response = Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn('Pantai', html_response)
        self.assertNotIn('Peserta', html_response)
        self.assertIn('Fun Banget', html_response)

    def test_post_activity(self):
        test = 'Agung'
        response_post = Client().post(path='/activity/saveactivity', data={'activity_name': test, 'description': test})

        response= Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_post_member(self):
        activity =  Activity.objects.create(activity_name='Pantai', description='Fun Banget')
        test = 'Agung'
        response_post = Client().post(path='/activity/savemember-Pantai', data={'name': test})

        response= Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertIn('Pantai', html_response)
        self.assertIn(test, html_response)

    def test_post_member_responsive(self):
        member = Member.objects.create(name='Agung')
        activity =  Activity.objects.create(activity_name='Pantai', description='')

        new_model = MemberActivity.objects.create(name=member, activity=activity)

        response_post = Client().post(path='/activity/savemember-Pantai', data={'name': 'Agung'}, follow=True)
        html_response = response_post.content.decode('utf8')
        self.assertIn('invalid', html_response)

    def test_post_member_responsive2(self):
        member = Member.objects.create(name='Agung')
        activity =  Activity.objects.create(activity_name='Pantai', description='')

        new_model = MemberActivity.objects.create(name=member, activity=activity)

        response_post = Client().post(path='/activity/savemember-Pantai', data={'name': 'Ihsan'}, follow=True)
        html_response = response_post.content.decode('utf8')
        self.assertIn('added', html_response)

    def test_delete_activity(self):
        test = 'Pantai'
        activity =  Activity.objects.create(activity_name=test, description='Fun Banget')

        response= Client().get('/activity/deleteactivity-Pantai')

        response= Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_delete_member(self):
        member = Member.objects.create(name='Agung')
        member2 = Member.objects.create(name='Ihsan')
        activity =  Activity.objects.create(activity_name='Pantai', description='')

        new_model = MemberActivity.objects.create(name=member, activity=activity)
        new_model = MemberActivity.objects.create(name=member2, activity=activity)

        response= Client().get('/activity/deletemember-Pantai.Agung')

        response= Client().get('/activity/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Agung', html_response)