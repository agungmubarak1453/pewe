from django import forms
from .models import *

class Activity_Form(forms.ModelForm):
	class Meta:
		model = Activity
		fields = '__all__'

	error_messages = {
		'required' : 'Please Type'
	}

class Member_Form(forms.Form):
	name = forms.CharField(max_length=30)

	def nameError(self, msg):
		self.add_error('name', msg)