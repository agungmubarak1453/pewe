from django.db import models

# Create your models here.

class Activity(models.Model):
    activity_name = models.CharField(max_length=30, primary_key=True)
    description = models.CharField(max_length=100, blank=True, null=False)

class Member(models.Model):
    name = models.CharField(max_length=30, primary_key=True)

class MemberActivity(models.Model):
    name = models.ForeignKey(Member, on_delete=models.CASCADE, related_name='memberactivitys', null=False)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='activitymembers', null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'activity'], name='composite unique')
        ]