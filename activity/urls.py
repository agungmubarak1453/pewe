from django.urls import include, path
from .views import *

app_name = 'activity'
urlpatterns = [
    path('', home, name='home'),
    path('activityform', activityform, name='activityform'),
    path('saveactivity', saveactivity),
    path('deleteactivity-<str:activity_name>', deleteActivity, name='deleteactivity'),
    path('memberform-<str:activity_name>', memberform, name='memberform'),
    path('savemember-<str:activity_name>', savemember, name='savemember'),
    path('deletemember-<str:activity_name>.<str:member_name>', deleteMember, name='deletemember'),
]