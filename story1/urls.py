from django.urls import include, path
from .views import *

app_name = 'story1'
urlpatterns = [
    path('', home, name='home'),
]