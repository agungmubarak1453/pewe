from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
	
# Create your tests here.
class story8test(TestCase):

	def test_home_urls_exist(self):
		response = Client().get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_home_template_is_right(self):
		response= Client().get('/story8/')
		self.assertTemplateUsed(response,'ajaxtest.html')

	def test_home_is_render_right(self):
		response= Client().get('/story8/')
		html_response = response.content.decode('utf8')
		self.assertIn('Ajax Test', html_response)
		self.assertIn('bookself', html_response)
		self.assertIn('input', html_response)
		self.assertIn('table', html_response)

	def test_searchbook_json(self):
		response = Client().get('/story8/searchbook?query=Elsa')
		json = response.json()
		self.assertNotEqual(json['totalItems'], 0)