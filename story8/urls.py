from django.urls import include, path
from .views import *

app_name = 'story8'
urlpatterns = [
    path('', home, name='home'),
    path('searchbook', searchBook, name='searchbook')
]