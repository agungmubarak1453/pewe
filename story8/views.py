from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
ttl = 'Pewe'

def home(request):
    response = {
        'ttl' : ttl,
        'title' : 'Ajax Test'
        }
    return render(request, 'ajaxtest.html', response)

def searchBook(request):
    query = request.GET.get('query', '')
    json = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query).json()
    return JsonResponse(json)