from django.urls import include, path
from .views import *

app_name = 'story7'
urlpatterns = [
    path('', home, name='home'),
]