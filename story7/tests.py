from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
    
# Create your tests here.
class story7test(TestCase):

    def test_home_urls_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_home_template_is_right(self):
        response= Client().get('/story7/')
        self.assertTemplateUsed(response,'accordiontest.html')

    def test_home_is_render_right(self):
        response= Client().get('/story7/')
        html_response = response.content.decode('utf8')
        self.assertIn('Accordion Test', html_response)
        self.assertIn('accordion', html_response)
        self.assertIn('Aktivitas Saat Ini', html_response)
        self.assertIn('Pengalaman Organisasi/Kepanitiaan', html_response)
        self.assertIn('Prestasi', html_response)